# Olimpiad Project

MIET olympiad project of 9"E" class. Includes documentation, 3D model, python and arduino code.
*   Add arduino files to the "arduino" folder.
*   Add documentation to the "documentation" folder (txt or doc/docx document type)
*   You can get python code from the "python" folder. 
*   3D model will be available in the "documentation" folder