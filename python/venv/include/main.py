# Импортируем объекты и библиотеки
import speech_recognition as sr, os, sys, webbrowser, pyaudio, time
from modules import First, Second, Third, Four

# Выбор задания
def menu():
    choice = input("Выберите номер задания:\n1)Номер 1\n2)Номер 2\n3)Номер 3\n")
    try:
        if int(choice) == 1:
            First.output('self')

        elif int(choice) == 2:
            Second.start('self')

        elif int(choice) == 3:
            if int(input("Выберите номер пункта:\n1)Номер 1\n2)Номер 2\n")) == 1:
                Third.start('self')
            else:
                Four.start('self')

        else:
            menu()
    except:
        menu()

# Вызываем функцию
menu()
exit()
